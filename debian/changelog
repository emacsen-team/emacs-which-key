emacs-which-key (3.6.0-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 19:59:36 +0900

emacs-which-key (3.6.0-1) unstable; urgency=medium

  * New upstream version 3.6.0
  * Refresh patches and drop patch to fix version number
  * d/copyright: Update copyright information

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 26 Feb 2022 11:38:53 +0500

emacs-which-key (3.5.4-1) unstable; urgency=medium

  * New upstream version 3.5.4
  * Refresh patches

 -- Lev Lamberov <dogsleg@debian.org>  Wed, 15 Dec 2021 11:26:05 +0500

emacs-which-key (3.5.3-1) unstable; urgency=medium

  * New upstream version 3.5.3
  * Refresh patch to clean documentation
  * Add patch to fix version number
  * d/control: Declare Standards-Version 4.6.0 (no changes needed)

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 24 Aug 2021 13:07:13 +0500

emacs-which-key (3.5.1-1) unstable; urgency=medium

  * New upstream version 3.5.1
  * Refresh patch to clean documentation
  * Add upstream metadata
  * d/control: Declare Standards-Verion 4.5.1 (no changes needed)
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Wed, 03 Feb 2021 12:55:31 +0500

emacs-which-key (3.5.0-1) unstable; urgency=medium

  * New upstream version 3.5.0
  * Drop patch to fix version number
  * Update patch to clean documentation
  * d/control: Declare Standards-Version 4.5.0 (no changes needed)
  * d/control: Migrate to debhelper-compat 13


 -- Lev Lamberov <dogsleg@debian.org>  Sat, 12 Sep 2020 18:28:29 +0500

emacs-which-key (3.4.0-1) unstable; urgency=medium

  * New upstream version 3.4.0
  * Migrate to dh 12 without d/compat
  * Refresh patch to clean documentation
  * Add patch to fix version number
  * d/control: Declare Standards-Version 4.4.1 (no changes needed)
  * d/control: Drop emacs25 from Enhances
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Mon, 20 Jan 2020 10:45:23 +0500

emacs-which-key (3.3.1-3) unstable; urgency=medium

  * Team upload.
  * Regenerate source package with quilt patches

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 14:58:57 -0300

emacs-which-key (3.3.1-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 09:31:06 -0300

emacs-which-key (3.3.1-1) unstable; urgency=medium

  * New upstream version 3.3.1
  * Refresh patch
  * d/control: Declare Standards-Version 4.2.1 (no changes needed)

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 15 Nov 2018 10:45:12 +0500

emacs-which-key (3.3.0-1) unstable; urgency=medium

  * New upstream version 3.3.0
  * d/control: Update Maintainer (to Debian Emacsen team)
  * d/control: Declare Standards-Version 4.1.5 (no changes needed)
  * d/control: Add Rules-Requires-Root: no

 -- Lev Lamberov <dogsleg@debian.org>  Mon, 09 Jul 2018 21:57:57 +0500

emacs-which-key (3.2.0-1) unstable; urgency=medium

  * Change Vcs-{Browser,Git} URL to salsa.debian.org
  * New upstream version 3.2.0
  * Declare Standards-Version 4.1.4 (no changes needed)

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 08 May 2018 09:44:38 +0500

emacs-which-key (3.1.0-1) unstable; urgency=medium

  * New upstream version 3.1.0
  * Refresh patch
  * Migrate to dh 11
  * d/control: Declare Standards-Version 4.1.3 (no changes needed)
  * d/control: Remove emacs24 form Enhances field
  * d/control: Remove Built-Using field
  * d/copyright: Reflect copyright changes
    - Copyright transferred to FSF

 -- Lev Lamberov <dogsleg@debian.org>  Mon, 29 Jan 2018 12:49:08 +0500

emacs-which-key (3.0.2-1) unstable; urgency=medium

  * New upstream version 3.0.2
  * Remove emacsen-common from Depends field
    - it is substituted by dh-elpa

 -- Lev Lamberov <dogsleg@debian.org>  Sun, 20 Aug 2017 00:00:50 +0500

emacs-which-key (3.0.1-1) unstable; urgency=medium

  * New upstream version 3.0.1
  * Refresh patch

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 20 Jun 2017 11:19:28 +0500

emacs-which-key (2.0-1) unstable; urgency=medium

  * Initial release (Closes: #854717)

 -- Lev Lamberov <dogsleg@debian.org>  Fri, 10 Feb 2017 00:21:51 +0500
